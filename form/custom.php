<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Initial values
$api_endpoint_phone = "https://www.bailtrac.com/dialer/getphonedata.aspx";
$api_endpoint_notes = "https://www.bailtrac.com/dialer/AddPhoneNote.aspx";

$user = "jltorres";
$phone_number = "8483910942";
$campaign = "2002";

$vendor_id = "";
if (isset($_POST["campaign"]))	{$campaign=$_POST["campaign"];}
	elseif (isset($_GET["campaign"]))	{$campaign=$_GET["campaign"];}
if (isset($_POST["user"]))	{$user=$_POST["user"];}
	elseif (isset($_GET["user"]))	{$user=$_GET["user"];}
if (isset($_POST["agent_log_id"]))	{$agent_log_id=$_POST["agent_log_id"];}
	elseif (isset($_GET["agent_log_id"]))	{$agent_log_id=$_GET["agent_log_id"];}
if (isset($_POST["agent"]))	{$agent=$_POST["agent"];}
	elseif (isset($_GET["agent"]))	{$agent=$_GET["agent"];}
if (isset($_POST["lead_id"]))	{$lead_id=$_POST["lead_id"];}
	elseif (isset($_GET["lead_id"]))	{$lead_id=$_GET["lead_id"];}
if (isset($_POST["vendor_id"]))	{$vendor_id=$_POST["vendor_id"];}
	elseif (isset($_GET["vendor_id"]))	{$vendor_id=$_GET["vendor_id"];}
	$vendor_lead_code = $vendor_id;
if (isset($_POST["list_id"]))	{$list_id=$_POST["list_id"];}
	elseif (isset($_GET["list_id"]))	{$list_id=$_GET["list_id"];}
if (isset($_POST["gmt_offset_now"]))	{$gmt_offset_now=$_POST["gmt_offset_now"];}
	elseif (isset($_GET["gmt_offset_now"]))	{$gmt_offset_now=$_GET["gmt_offset_now"];}
if (isset($_POST["phone_code"]))	{$phone_code=$_POST["phone_code"];}
	elseif (isset($_GET["phone_code"]))	{$phone_code=$_GET["phone_code"];}
if (isset($_POST["phone_number"]))	{$phone_number=$_POST["phone_number"];}
	elseif (isset($_GET["phone_number"]))	{$phone_number=$_GET["phone_number"];}
if (isset($_POST["title"]))	{$title=$_POST["title"];}
	elseif (isset($_GET["title"]))	{$title=$_GET["title"];}
if (isset($_POST["first_name"]))	{$first_name=$_POST["first_name"];}
	elseif (isset($_GET["first_name"]))	{$first_name=$_GET["first_name"];}
if (isset($_POST["middle_initial"]))	{$middle_initial=$_POST["middle_initial"];}
	elseif (isset($_GET["middle_initial"]))	{$middle_initial=$_GET["middle_initial"];}
if (isset($_POST["last_name"]))	{$last_name=$_POST["last_name"];}
	elseif (isset($_GET["last_name"]))	{$last_name=$_GET["last_name"];}
if (isset($_POST["address1"]))	{$address1=$_POST["address1"];}
	elseif (isset($_GET["address1"]))	{$address1=$_GET["address1"];}
if (isset($_POST["address2"]))	{$address2=$_POST["address2"];}
	elseif (isset($_GET["address2"]))	{$address2=$_GET["address2"];}
if (isset($_POST["address3"]))	{$address3=$_POST["address3"];}
	elseif (isset($_GET["address3"]))	{$address3=$_GET["address3"];}
if (isset($_POST["city"]))	{$city=$_POST["city"];}
	elseif (isset($_GET["city"]))	{$city=$_GET["city"];}
if (isset($_POST["state"]))	{$state=$_POST["state"];}
	elseif (isset($_GET["state"]))	{$state=$_GET["state"];}
if (isset($_POST["province"]))	{$province=$_POST["province"];}
	elseif (isset($_GET["province"]))	{$province=$_GET["province"];}
if (isset($_POST["postal_code"]))	{$postal_code=$_POST["postal_code"];}
	elseif (isset($_GET["postal_code"]))	{$postal_code=$_GET["postal_code"];}
if (isset($_POST["country_code"]))	{$country_code=$_POST["country_code"];}
	elseif (isset($_GET["country_code"]))	{$country_code=$_GET["country_code"];}
if (isset($_POST["gender"]))	{$gender=$_POST["gender"];}
	elseif (isset($_GET["gender"]))	{$gender=$_GET["gender"];}
if (isset($_POST["date_of_birth"]))	{$date_of_birth=$_POST["date_of_birth"];}
	elseif (isset($_GET["date_of_birth"]))	{$date_of_birth=$_GET["date_of_birth"];}
if (isset($_POST["alt_phone"]))	{$alt_phone=$_POST["alt_phone"];}
	elseif (isset($_GET["alt_phone"]))	{$alt_phone=$_GET["alt_phone"];}
if (isset($_POST["email"]))	{$email=$_POST["email"];}
	elseif (isset($_GET["email"]))	{$email=$_GET["email"];}
if (isset($_POST["security_phrase"]))	{$security_phrase=$_POST["security_phrase"];}
	elseif (isset($_GET["security_phrase"]))	{$security_phrase=$_GET["security_phrase"];}
if (isset($_POST["comments"]))	{$comments=$_POST["comments"];}
  elseif (isset($_GET["comments"]))	{$comments=$_GET["comments"];}
  
  require "php/api.php";

  $api = new api();
  
  $logo = "img/bhl_logo.svg";

  // Set API endpoint depending of Campaing ID
  if($campaign == "2002" || $campaign == "2003" || $campaign == "2004")
  {
    $logo = "img/liberty_logo.png";
    $api_endpoint_phone = "https://www.bailtrac.com/liberty/dialer/getphonedata.aspx";
    // $api_endpoint_notes = "https://www.bailtrac.com/liberty/dialer/AddPhoneNote.aspx";
  }

  // Get Client information
  $json_client = $api->GET($api_endpoint_phone,"pw=12ajKwp45$&ph=".$phone_number);
  $response = json_decode( $json_client, true );
  // echo json_encode($response);

  // Enviar Nota
  $note = "ViCi Dial";
  $api->notes($api_endpoint_notes,$user,$phone_number,$note);

?>
<!doctype html>
  <head>

    <link rel="stylesheet" href="gridforms/gridforms.css" type="text/css">
  </head>
  <body>

    <img style="margin-bottom: 30px; height: 50%; width: 50%; transform: translate(50%, 0);" src="<?php echo $logo ?>"/>

    <div class="grid-form">
      <fieldset>
        <legend>Primary Phone</legend>
        <div data-row-span="1">
          <div data-field-span="1">
            <label>Phone</label>
            <input id="txt_phone_number" type="text" value="<?php echo $phone_number ?>" autofocus>
          </div>
        </div>
      </fieldset>
      <br><br>

      <fieldset>
        <legend>Contact Information</legend>
        <div data-row-span="4">
          <div data-field-span="3">
            <label>Contact</label>
            <input id="txt_contact" type="text" value="<?php echo $response[0]["sContact"] ?>" autofocus>
          </div>
          <div data-field-span="1">
            <label>Type</label>
            <input id="txt_type" type="text" value="<?php echo $response[0]["sType"] ?>" autofocus>
          </div>
        </div>
        <div data-row-span="3">
          <div data-field-span="1">
            <label>Primary Phone</label>
            <input id="txt_primary_phone" type="text" value="<?php echo $response[0]["sPhone"] ?>" autofocus>
          </div>
          <div data-field-span="1">
            <label>SSN</label>
            <input id="txt_ssn" type="text" value="<?php echo $response[0]["sSSN"] ?>" autofocus>
          </div>
          <div data-field-span="1">
            <label>Date of Birth</label>
            <input id="txt_dob" type="text" value="<?php echo $response[0]["dtDOB"] ?>" autofocus>
          </div>
        </div>
        <div data-row-span="2">
          <div data-field-span="1">
            <label>Defendant</label>
            <input id="txt_defendant" type="text" value="<?php echo $response[0]["sDefendant"] ?>" autofocus>
          </div>
          <div data-field-span="1">
            <label>Post Date</label>
            <input id="txt_post_date" type="text" value="<?php echo $response[0]["dtTrans_Executed"] ?>" autofocus>
          </div>
        </div>
      </fieldset>
      <br><br>

      <fieldset>
        <legend>Bond Information</legend>
        <div data-row-span="4">
          <div data-field-span="1">
            <label>Bond Amount</label>
            <input id="txt_bond_amount" type="text" value="$<?php echo $response[0]["mTrans_Amount"] ?>" autofocus>
          </div>
          <div data-field-span="1">
            <label>Bond Identifier</label>
            <input id="txt_bond_identifier" type="text" value="<?php echo $response[0]["sIdentifier"] ?>" autofocus>
          </div>
          <div data-field-span="1">
            <label>Office</label>
            <input id="txt_office" type="text" value="<?php echo $response[0]["sOffice"] ?>" autofocus>
          </div>
          <div data-field-span="1">
            <label>Agent</label>
            <input id="txt_agent" type="text" value="<?php echo $response[0]["sAgent"] ?>" autofocus>
          </div>
        </div>
      </fieldset>
      <br><br>

      <fieldset>
        <legend>Payment Information</legend>
        <div data-row-span="4">
          <div data-field-span="1">
            <label>Calc Days Overdue</label>
            <input id="txt_days_overdue" type="text" value="<?php echo $response[0]["iDays_Overdue"] ?>" autofocus>
          </div>
          <div data-field-span="1">
            <label>Past Due Balance</label>
            <input id="txt_past_due_balance" type="text" value="$<?php echo $response[0]["mPast_Due"] ?>" autofocus>
          </div>
          <div data-field-span="1">
            <label>Current Due</label>
            <input id="txt_current_due" type="text" value="$<?php echo $response[0]["mCurrent_Due"] ?>" autofocus>
          </div>
          <div data-field-span="1">
            <label>Total Balance</label>
            <input id="txt_total_balance" type="text" value="$<?php echo $response[0]["mTotal_Balance"] ?>" autofocus>
          </div>
        </div>
      </fieldset>
      <br><br>

      <fieldset>
        <legend>Comments</legend>
        <div data-row-span="1">
          <div data-field-span="1">
            <label>Comment</label>
            <textarea id="txt_comment" autofocus><?php echo $user ?> currently in session in Campaign <?php echo $campaign ?> API URL: <?php echo $api_endpoint_phone ?></textarea>
          </div>          
        </div>
      </fieldset>
      <br><br>
    </div>
    <!-- Form example -->

    <script src="js/jquery.min.js"></script>
    <script src="gridforms/gridforms.js"></script>
    <script src="js/cleave.min.js"></script>
    <script src="js/form.js"></script>
  </body>
</html>