$(document).ready(function(){
  
  $("#lnk_save_form_changes").on("click",async function(){
    var Accion = "notes";
    var agent = $(this).attr("data-agent");
    var phone_number = $(this).attr("data-phone-number");
    var note = $("#txt_comment").val();
    if(note == "")
      note = "ViCi Dial";
      
    let formData = {Accion,agent,phone_number,note};

    $.post("/agc/vicidial_form/form/php/functions.php",formData,function(data){
      console.log(data);
    },"JSON");
    alert("Agent: "+agent+" is sending Note: "+note);
  });

  function form_data()
  {
    let data = [];
    let divElem = document.querySelector(".grid-form");
    let inputs = divElem.querySelectorAll("input, select, checkbox, textarea");

    // Iterate over the form controls
    for (let i = 0; i < inputs.length; i++)
    {
      if(inputs[i].nodeName != "FIELDSET")
        if (inputs[i].nodeName === "INPUT" && inputs[i].type === "radio")
        {
          if(inputs[i].checked)
            data.push({"element":inputs[i].nodeName,"type":inputs[i].type,"id":inputs[i].name,"value":inputs[i].value});
        }
        else
          data.push({"element":inputs[i].nodeName,"type":inputs[i].type,"id":inputs[i].id,"value":inputs[i].value});
    }

    return data;
  }

  function form_data_clean()
  {
    let divElem = document.querySelector(".grid-form");
    let inputs = divElem.querySelectorAll("input, select, checkbox, textarea");

    // Iterate over the form controls
    for (let i = 0; i < inputs.length; i++)
    {
      if(inputs[i].nodeName != "FIELDSET")
        if (inputs[i].nodeName === "INPUT" && inputs[i].type === "radio")
        {
          if(inputs[i].checked)
            inputs[i].checked = false;
        }
        else
          inputs[i].value = '';
    }
  }

});

class Main
{
  static post(url,params={})
  {
    return fetch(url,{
      method: 'POST',
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json'
      },
      credentials: 'same-origin',
      body: JSON.stringify(params)
    });
  }
}