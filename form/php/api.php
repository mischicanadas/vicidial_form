<?php

class api
{  
  function GET($URL,$DATA)
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $URL,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => false,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_POSTFIELDS =>    $DATA,
    ));

    $Response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) 
    {
      $Response = $err;
    } 
    
    return $Response;
  }

  function POST($URL,$DATA)
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $URL,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => false,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS =>    $DATA,
    ));

    $Response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) 
    {
      $Response = $err;
    } 
    
    return $Response;
  }

  function Listar_Usuarios()
  {
    $Query = "select user_id,user,pass,full_name from vicidial_users";
		return $this->Ejecuta_Query($Query);
  }

  function notes($api_end_point,$agent,$phone_number,$note)
  {
    if($note == "")
    {
      // Add Automatic Note
      $note = "ViCi Dial";
    }
    
    $notes_response = $this->POST($api_end_point,"pw=12ajKwp45$&ph=".$phone_number."&subject=Vici Dial&note=".$note."&user=".$agent);
    if($notes_response == 1)
      $notes_response = "Note was added";
    else
      $notes_response = "Error saving note";
      
    return $notes_response;
  }

  function Ejecuta_Query($Query)
  {
    include("ConnDB.php");

    $this->ConnDB = $ConnDB;
    $Conexion = new mysqli($ConnDB["Servidor"],$ConnDB["Usuario"],$ConnDB["Password"],$ConnDB["DB"]);
    if ($Conexion->connect_error) 
    {
      die("Connection failed: " . $Conexion->connect_error);
    }

    $result= $Conexion->query($Query);
    
    $Conexion->close();
    return $result;
  }

}
?>