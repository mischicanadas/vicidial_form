<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header("Content-Type: application/json", true);

require "api.php";
$api = new api();

$Regresa = $_POST["agent"];

// Handles AJAX Call
if (isset($_POST["Accion"]))
{
  $Accion = $_POST['Accion'];
  
  switch($Accion)
  {
    case "notes":
    {
      $agent = $_POST['agent'];
      $phone_number = $_POST['phone_number'];
      $note = $_POST['note'];

      $Regresa = $api->notes($agent,$phone_number,$note);
      
      break;
    }
  }
}

echo json_encode($Regresa);

?>